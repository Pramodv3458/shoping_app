package com.example.eshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static androidx.core.content.ContextCompat.startActivity;

public class Signup extends AppCompatActivity {
    EditText email, password, name, mob, repassword,id;
    private static final String url = "jdbc:mysql://27.56.206.23:3306/eshop";
    private static final String user = "root";
    private static final String pass = "pramod";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        mob = findViewById(R.id.mob);
        name = findViewById(R.id.name);
        repassword = findViewById(R.id.repassword);
        id =findViewById(R.id.id);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ConnectMySql connectMySql = new ConnectMySql();
        connectMySql.execute("");
    }

    public void login(View view) {
        if (name.getText().toString().isEmpty()) {
            name.setError("enter name ");
            name.requestFocus();
        } else if (email.getText().toString().isEmpty()) {
            email.setError("Enter email address");
            email.requestFocus();
        } else if (password.getText().toString().isEmpty()) {
            password.setError("enter password");
            password.requestFocus();
        } else if (!repassword.getText().toString().equals(password.getText().toString())) {
            repassword.setError("not match password");
            repassword.requestFocus();
        } else if (mob.getText().toString().isEmpty()) {
            mob.setError("enter mobile number");
            mob.requestFocus();
        } else {
        }

    }

    public void log(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public class ConnectMySql extends AsyncTask<String, Void, String> {
        String res = "";
        String i = id.getText().toString();
        String e = email.getText().toString();
        String p = password.getText().toString();
        String n = name.getText().toString();
        String repass = repassword.getText().toString();
        String m = mob.getText().toString();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:mysql://27.56.206.23:3306/eshop", "root", "pramod");
                System.out.println("Databaseection success");

                String result = "Database Connection Successful\n";
                Statement st = con.createStatement();
                st.executeUpdate("insert into user values" +id+n+e+p+m);
            } catch (Exception e) {
                e.printStackTrace();
                res = e.toString();
            }
            return res;
        }
        @Override
        protected void onPostExecute(String result) {
           // txtData.setText(result);
        }
    }
}

